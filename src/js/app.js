import { randomUserMock, additionalUsers } from "./FE4U-Lab2-mock.js";
let users;
const courses = [
  "Mathematics",
  "Physics",
  "English",
  "Computer Science",
  "Dancing",
  "Chess",
  "Biology",
  "Chemistry",
  "Law",
  "Art",
  "Medicine",
  "Statistics",
];

//генерує випадковий предмет курсу з заданого списку
function randomCourse() {
  let number = Math.floor(Math.random() * courses.length);
  return courses[number];
}

//модифікує об'єкт користувача, який був в mock, додаючи/змінюючи певні властивості, щоб задовольнити вимоги, описані в першому завданні
function modifyUser(userObject) {
  let modifiedObject = {};
  modifiedObject.gender = userObject.gender;
  modifiedObject.title = userObject.name.title;
  modifiedObject.full_name = userObject.name.first + " " + userObject.name.last;
  modifiedObject.city = userObject.location.city;
  modifiedObject.state = userObject.location.state;
  modifiedObject.country = userObject.location.country;
  modifiedObject.postcode = userObject.location.postcode;
  modifiedObject.coordinates = userObject.location.coordinates;
  modifiedObject.timezone = userObject.location.timezone;
  modifiedObject.email = userObject.email;
  modifiedObject.b_date = userObject.dob.date;
  modifiedObject.age = userObject.dob.age;
  modifiedObject.phone = userObject.phone;
  modifiedObject.picture_large = userObject.picture.large;
  modifiedObject.picture_thumbnail = userObject.picture.thumbnail;
  modifiedObject.id = userObject.id.name + userObject.id.value;
  modifiedObject.favorite = getRandomBoolean();
  modifiedObject.bg_color = "#1f75cb";
  modifiedObject.note = "Note";

  modifiedObject.course = randomCourse();

  return modifiedObject; //повертає новий об'єкт користувача.
}

//рандомно визначаємо, хто з викладачів улюблений
function getRandomBoolean() {
  return Math.round(Math.random() * 2) == 0;
}

//застосовує modifyUser до кожного об'єкта в масиві користувачів.
function modifyUsersArray(array) {
  return array.map((user) => modifyUser(user));
}

//створює об'єкт, який є об'єднанням масивів користувачів з randomUserMock та additionalUsers.
const mergedObject = [
  ...modifyUsersArray(randomUserMock),
  ...additionalUsers,
].filter((user) => validateUser(user));

//видаляє дублікати користувачів у масиві за їхнім повним ім'ям.
function deleteDuplicates(array) {
  const namesArray = array.map((user) => user.full_name);
  let duplicatesIndexes = [];
  //перевіряє кожне ім'я в namesArray та додає індекс до duplicatesIndexes, якщо знаходить дублікат.
  namesArray.forEach((name, i) => {
    const otherNames = namesArray.slice(i + 1);
    if (otherNames.includes(name)) {
      duplicatesIndexes.push(i);
    }
  });

  return array.filter((user, i) => !duplicatesIndexes.includes(i));
}

function validateUser(userObject) {
  if (
    typeof userObject.full_name === "string" &&
    userObject.full_name[0] === userObject.full_name[0].toUpperCase() &&
    typeof userObject.gender === "string" &&
    typeof userObject.note === "string" &&
    userObject.note[0] === userObject.note[0].toUpperCase() &&
    typeof userObject.city === "string" &&
    userObject.city[0] === userObject.city[0].toUpperCase() &&
    typeof userObject.country === "string" &&
    userObject.country[0] === userObject.country[0].toUpperCase() &&
    typeof userObject.age === "number" &&
    validatePhone(userObject.phone, userObject.country) &&
    validateEmail(userObject.email)
  ) {
    return true; //валідація пройшла успішно
  } else {
    return false; //об'єкт не відповідає вимогам
  }
}

//валідація номеру телефону для кожної країни згідно з тими номерами, які подані у randomUserMock
function validatePhone(phone, country) {
  const phoneFormats = {
    Germany: /^\d{4}[-\s]?\d{7}$/,
    Ireland: /^\d{3}[-\s]?\d{3}[-\s]?\d{4}$/,
    Australia: /^\d{2}[-\s]?\d{4}[-\s]?\d{4}$/,
    "United States": /^\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{4}$/,
    Finland: /^0\d{1}[-\s]?\d{3}[-\s]?\d{3}$/,
    Turkey: /^\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{4}$/,
    Switzerland: /^0\d{2}[-\s]?\d{3}[-\s]?\d{2}[-\s]?\d{2}$/,
    "New Zealand": /^\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{4}$/,
    Spain: /^\d{3}[-\s]?\d{3}[-\s]?\d{3}$/,
    Norway: /^\d{8}$/,
    Denmark: /^\d{8}$/,
    Iran: /^\d{3}[-\s]?\d{8}$/,
    Canada: /^\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{4}$/,
    Netherlands: /^\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{4}$/,
    France: /^\d{2}[-\s]?\d{2}[-\s]?\d{2}[-\s]?\d{2}[-\s]?\d{2}$/,
  };

  return phoneFormats[country]
    ? phoneFormats[country].test(phone)
    : /^[\d\s-]{7,15}$/.test(phone);
}

function validateEmail(email) {
  return email.includes("@");
}

//використовує функцію filter для створення нового масиву користувачів, які відповідають умовам фільтра.
//порівнює кожного користувача з об'єктом filters та включає його в результат, якщо всі умови виконуються.
function filterUsers(usersArray, filters) {
  return usersArray.filter((user) => {
    return (
      (!filters.country || user.country === filters.country) &&
      (!filters.age ||
        (user.age >= filters.age[0] && user.age <= filters.age[1])) &&
      (!filters.gender || user.gender === filters.gender) &&
      (!filters.favorite || user.favorite) &&
      (!filters.withPhoto || user.picture_large)
    );
  });
}

//сортує масив користувачів за вказаним порядком та полем
function sortUsers(usersArray, sortOrder, sortBy) {
  return usersArray.sort((a, b) => {
    //працює як з числовими, так і з рядковими значеннями, перетворюючи рядки в верхній регістр для правильного порівняння
    const valueA =
      typeof a[sortBy] === "string" ? a[sortBy].toUpperCase() : a[sortBy];
    const valueB =
      typeof b[sortBy] === "string" ? b[sortBy].toUpperCase() : b[sortBy];

    //повертає новий масив користувачів, відсортований за вказаним полем та порядком
    if (valueA < valueB) {
      return -1 * sortOrder;
    } else if (valueA > valueB) {
      return 1 * sortOrder;
    } else {
      return 0;
    }
  });
}

//якщо searchParameter є рядком, то перевіряє чи є рядок діапазоном віку, якщо ні - здійснює пошук за іменем та приміткою.
//якщо searchParameter - число, то проводить пошук за віком.
//повертає новий масив об'єктів, що відповідають заданим умовам пошуку.
function searchObjectsByParameter(objectsArray, searchParameter) {
  return objectsArray.filter((object) => {
    if (typeof searchParameter === "string") {
      let ageRange = searchParameter.split("-");
      if (
        ageRange.length > 1 &&
        typeof parseInt(ageRange[0]) === "number" &&
        typeof parseInt(ageRange[1]) === "number"
      ) {
        return (
          object.age >= parseInt(ageRange[0]) &&
          object.age <= parseInt(ageRange[1])
        );
      }

      return (
        (object.full_name &&
          typeof searchParameter === "string" &&
          object.full_name
            .toLowerCase()
            .includes(searchParameter.toLowerCase())) ||
        (object.note &&
          typeof searchParameter === "string" &&
          object.note.toLowerCase().includes(searchParameter.toLowerCase()))
      );
    } else if (object.age && typeof searchParameter === "number") {
      return object.age == searchParameter;
    } else return false;
  });
}

//розраховує відсоток збігу об'єктів за заданим параметром.
//обчислює загальну кількість об'єктів у вхідному масиві.
//розраховує відсоток збігу та повертає його у вигляді рядка зі знаком відсотка.
function calculatePercentageMatching(objectsArray, searchParameter) {
  const numberOfMatches = searchObjectsByParameter(
    objectsArray,
    searchParameter
  ).length;
  const totalObjects = objectsArray.length;

  const percentage = ((numberOfMatches / totalObjects) * 100).toFixed(2);
  return percentage + "%";
}

users = deleteDuplicates(mergedObject);
console.log("users", users);

console.log(
  "filter:",
  filterUsers(users, {
    gender: "male",
  })
);

console.log("sort:", sortUsers(users, -1, "age"));

console.log("search:", searchObjectsByParameter(users, "re"));

console.log("search percentage:", calculatePercentageMatching(users, "28-40"));

//ІНФОРМАЦІЯ ПРО ВИКЛАДАЧА (POPUP)

//отримуємо елементи з html
const teachersListElement = document.getElementById("teachers");
const favoriteTeacherFigures = document.querySelector(".favorites-figures");
const teacherInfoPopup = document.getElementById("teacher-info-popup");

//функція для відображення інформації про вчителя в спливаючому вікні
function renderTeacherInfo(teacher, teacherNode) {
  const teacherPhoto = teacherInfoPopup.querySelector(
    ".teacher-photo-info-popup"
  );
  //перевіряє, чи є у вчителя фото, якщо ні - ставимо дефолтне фото юзера
  if (teacher.picture_large) {
    teacherPhoto.src = teacher.picture_large;
    teacherPhoto.alt = "Teacher`s photo";
  } else {
    teacherPhoto.src = "images/user-icon.png";
    teacherPhoto.alt = "Default user icon";
  }

  //оновлює ім'я і предмет вчителя у спливаючому вікні
  const teacherName = teacherInfoPopup.querySelector("h1");
  const teacherCourse = teacherInfoPopup.querySelector("h3");
  teacherName.textContent = teacher.full_name;
  teacherCourse.textContent = teacher.course;

  //оновлює розділ адреси у спливаючому вікні
  //(місто, країна, вік, стать, електронна пошта та телефон)
  const addressParagraphs = teacherInfoPopup.querySelectorAll("address p");
  addressParagraphs[0].textContent = teacher.city
    ? `${teacher.city}, ${teacher.country}`
    : teacher.country;
  addressParagraphs[1].textContent = teacher.age
    ? `${teacher.age}, ${teacher.gender}`
    : teacher.gender;
  addressParagraphs[2].textContent = teacher.email;
  addressParagraphs[3].textContent = teacher.phone;

  //оновлює абзац з описом
  const descriptionParagraph = teacherInfoPopup.querySelector(".description p");
  descriptionParagraph.textContent = teacher.note || "";

  //оновлює іконку зірочки в залежності від того, чи є вчитель улюбленим
  const starIcon = teacherInfoPopup.querySelector(".star");
  starIcon.src = teacher.favorite ? "images/star.png" : "images/empty-star.png";
  starIcon.removeEventListener("click", toggleFavorite);

  //встановлюємо новий слухач подій для кліку на зірочку
  starIcon.onclick = toggleFavorite;

  //функція для перемикання вчителя як улюбленого
  function toggleFavorite() {
    addToFavorites(teacher);
    //змінює src starIcon в залежності від того, чи вчитель вже є у списку улюблених
    starIcon.src = teacher.favorite
      ? "images/star.png"
      : "images/empty-star.png";
    //оновлюємо відображення списку учителів та улюблених
    displayTeachers(users);
    displayFavoriteTeachers(users);
  }
}

//РЕНДЕРИНГ ОБ'ЄКТІВ ВИКЛАДАЧІВ

function createTeacherFigure(teacher, displayedInFavorites) {
  //створення елементу figure
  const figure = document.createElement("figure");
  figure.className = "teacher-figure";
  figure.setAttribute("data-index", users.indexOf(teacher));

  //створення контейнера для фото і зірки
  const photoStarDiv = document.createElement("div");
  photoStarDiv.className = "photo-star-container";

  //контейнер для фотографії вчителя, на який вішаємо EventListener,
  //щоб відкривалося вспливаюче вікно з деталями про нього
  const photoDiv = document.createElement("div");
  photoDiv.className = "teacher-photo";
  photoDiv.addEventListener("click", () => {
    openPopup(teacherInfoPopup);
    renderTeacherInfo(teacher, figure);
  });

  //перевіряємо, чи є у вчителя фото
  if (teacher.picture_large) {
    const img = document.createElement("img");
    img.src = teacher.picture_large;
    img.alt = "Teacher`s photo";
    photoDiv.appendChild(img);
  } else {
    //якщо немає - створюється елемент "span" з класом "teacher-initials",
    //встановлюються ініціали з повного імені вчителя
    const initialsSpan = document.createElement("span");
    initialsSpan.className = "teacher-initials";
    const initials = teacher.full_name
      .split(" ")
      .map((name) => name.charAt(0).toUpperCase())
      .join("");
    initialsSpan.textContent = initials;
    photoDiv.appendChild(initialsSpan);
  }

  photoStarDiv.appendChild(photoDiv);

  //якщо вчитель улюблений - додаємо фото зірки
  if (teacher.favorite && !displayedInFavorites) {
    const starImg = document.createElement("img");
    starImg.src = "images/star.png";
    starImg.alt = "star icon";
    starImg.className = "star";
    photoStarDiv.appendChild(starImg);
  }
  figure.appendChild(photoStarDiv);

  //створюємо figcaption
  const figcaption = document.createElement("figcaption");

  //створення і додавання абзаців з іменем, предметом і країною вчителя до figcaption
  const nameParagraph = document.createElement("p");
  nameParagraph.className = "teacher-name";
  nameParagraph.textContent = teacher.full_name;
  figcaption.appendChild(nameParagraph);

  const subjectParagraph = document.createElement("p");
  subjectParagraph.className = "teacher-subject";
  subjectParagraph.textContent = teacher.course;
  if (!displayedInFavorites) figcaption.appendChild(subjectParagraph);

  const countryParagraph = document.createElement("p");
  countryParagraph.className = "teacher-country";
  countryParagraph.textContent = teacher.country;
  figcaption.appendChild(countryParagraph);

  figure.appendChild(figcaption);

  //повертається створений елемент figure з усім вмістом.
  return figure;
}

function displayTeachers(users) {
  //очищаємо вміст елементу перед вставкою нових даних
  teachersListElement.innerHTML = "";

  //для кожного вчителя викликає функцію createTeacherFigure, яка повертає DOM-елемент, що представляє вчителя
  users.forEach((teacher) => {
    const teacherElement = createTeacherFigure(teacher, false);

    teachersListElement.appendChild(teacherElement);
  });
}

function displayFavoriteTeachers(users) {
  //очищаємо вміст елементу перед вставкою нових даних
  favoriteTeacherFigures.innerHTML = "";

  //фільтруємо вчителів за умовою favorite і відображаємо їх
  users
    .filter((user) => user.favorite)
    .forEach((teacher) => {
      const teacherElement = createTeacherFigure(teacher, true);

      favoriteTeacherFigures.appendChild(teacherElement);
    });
}

// додавання викладача до обраних
function addToFavorites(teacher) {
  if (teacher) {
    teacher.favorite = !teacher.favorite;
    console.log(`${teacher.full_name} favorite: ${teacher.favorite}`);
  }
}

//ФІЛЬТРИ

//отримуємо елементи фільтрів
const ageFilter = document.getElementById("ageFilter");
const countryFilter = document.getElementById("regionFilter");
const genderFilter = document.getElementById("sexFilter");
const withPhotoFilter = document.getElementById("withPhoto");
const onlyFavoritesFilter = document.getElementById("onlyFavorites");

//фільтрація вчителів за заданим параметром
function filterTeachers(users) {
  //ініціалізуємо об'єкт фільтра з використанням значень фільтрів
  let filter = {
    age: getAgeRange(ageFilter.value),
    favorite: onlyFavoritesFilter.checked,
    withPhoto: withPhotoFilter.checked,
  };

  //додаємо до фільтра значення статі та країни, якщо воно не "all"
  if (genderFilter.value !== "all")
    filter.gender = genderFilter.value.toLowerCase();
  if (countryFilter.value !== "all") filter.country = countryFilter.value;

  //використовуємо функцію фільтрації користувачів та отримуємо відфільтрованих вчителів
  let filteredTeachers = filterUsers(users, filter);

  console.log(filter);
  console.log(filteredTeachers);

  //виводимо відфільтрованих вчителів на сторінку
  displayTeachers(filteredTeachers);
  sortTable(filteredTeachers);
}

//скидання значень всіх фільтрів до значень за замовчуванням.
function resetFilters() {
  ageFilter.value = "all";
  countryFilter.value = "all";
  genderFilter.value = "all";
  withPhotoFilter.checked = false;
  onlyFavoritesFilter.checked = false;
}

//отримання діапазону віку на основі вибраного значення фільтра
function getAgeRange(filter) {
  switch (filter) {
    case "18-31":
      return [18, 31];
    case "32-44":
      return [32, 44];
    case "45-54":
      return [45, 54];
    case "55-64":
      return [55, 64];
    case "65+":
      return [65, Infinity];
    default:
      return [0, Infinity];
  }
}

//обробка фільтрів на основі заданого масиву вчителів
function handleFilters(array) {
  //додаємо слухачі подій для виклику фільтрації при зміні параметрів
  ageFilter.addEventListener("change", () => filterTeachers(array));
  countryFilter.addEventListener("change", () => filterTeachers(array));
  genderFilter.addEventListener("change", () => filterTeachers(array));
  withPhotoFilter.addEventListener("change", () => filterTeachers(array));
  onlyFavoritesFilter.addEventListener("change", () => filterTeachers(array));

  //викликаємо функцію фільтрації з початковими значеннями масиву вчителів
  filterTeachers(array);
}

//СОРТУВАННЯ

//отримуємо елемент таблиці для відображення статистики
const table = document.querySelector(".table-statistics");

//функція для сортування таблиці вчителів та навігації по сторінках
function sortTable(array) {
  let sortedUsers = array;
  let pageNumber = 1;

  //отримуємо елемент таблиці та її заголовок
  const tableHead = table.querySelector("thead");

  //додаємо слухача подій для кожного заголовка стовпця
  tableHead.onclick = function (event) {
    const clickedElement = event.target;
    if (clickedElement.tagName === "TH") {
      // отримуємо індекс стовпця, який клікнули
      const sortParameters = [
        "full_name",
        "course",
        "age",
        "gender",
        "country",
      ];
      const columnIndex = Array.from(
        clickedElement.parentNode.children
      ).indexOf(clickedElement);

      //отримуємо напрямок сортування для даного стовпця
      let sortOrder = parseInt(clickedElement.dataset.sortorder);
      console.log(sortOrder);

      //змінюємо напрямок сортування та відображаємо відсортовану таблицю
      if (sortOrder === 1) {
        clickedElement
          .querySelector(".table-arrow")
          .classList.remove("rotated-arrow");
      } else {
        clickedElement
          .querySelector(".table-arrow")
          .classList.add("rotated-arrow");
      }
      //змінюємо напрямок сортування
      clickedElement.dataset.sortorder = sortOrder *= -1;

      //оновлюємо таблицю за допомогою відсортованого масиву користувачів
      sortedUsers = sortUsers(array, sortOrder, sortParameters[columnIndex]);
      renderTable(sortedUsers, pageNumber);
    }
  };

  //відображаємо відсортовану таблицю та викликаємо функцію навігації
  renderTable(sortedUsers, pageNumber);
  handleTableNavigation(sortedUsers);

  function handleTableNavigation(sortedUsers) {
    const navigation = document.querySelector(".page-number");
    navigation.innerHTML = "";

    //визначаємо кількість сторінок відповідно до кількості відфільтрованих вчителів
    const numberOfPages =
      sortedUsers.length % 10 === 0
        ? sortedUsers.length / 10
        : sortedUsers.length / 10 + 1;

    //додаємо елементи навігації для кожної сторінки
    for (let i = 1; i <= numberOfPages; i++) {
      const span = document.createElement("span");
      span.classList.add(i === 1 ? "selected-page" : "unselected-page");
      span.innerText = i;
      navigation.appendChild(span);

      //додаємо слухача події для зміни сторінки при кліку
      span.addEventListener("click", () => {
        //скидаємо класи для всіх сторінок та встановлюємо виділену сторінку
        navigation.querySelectorAll("span").forEach(resetClasses);
        span.classList.add("selected-page");
        span.classList.remove("unselected-page");

        //оновлюємо номер поточної сторінки та відображаємо відповідну частину таблиці
        pageNumber = i;
        renderTable(sortedUsers, i);
      });
    }

    //скидання класів стилів для всіх елементів сторінок
    function resetClasses(span) {
      span.classList.remove("selected-page");
      span.classList.add("unselected-page");
    }
  }
}

//РЕНДЕРИНГ КОРИСТУВАЧІВ В ТАБЛИЦЮ

//відображення певної сторінки таблиці користувачів
function renderTable(array, page) {
  const statisticsTable = table.querySelector("tbody");
  statisticsTable.innerHTML = "";

  //функція для створення та додавання рядка в таблицю
  function addUserToTable(user) {
    const row = document.createElement("tr");

    //ім'я
    const nameCell = document.createElement("td");
    nameCell.textContent = user.full_name;
    row.appendChild(nameCell);

    //спеціальність
    const specialityCell = document.createElement("td");
    specialityCell.textContent = user.course;
    row.appendChild(specialityCell);

    //вік
    const ageCell = document.createElement("td");
    ageCell.textContent = user.age;
    row.appendChild(ageCell);

    //стать
    const genderCell = document.createElement("td");
    genderCell.textContent = user.gender;
    row.appendChild(genderCell);

    //національність
    const nationalityCell = document.createElement("td");
    nationalityCell.textContent = user.country;
    row.appendChild(nationalityCell);

    //додавання рядка до таблиці
    statisticsTable.appendChild(row);
  }

  //додаємо кожного користувача до таблиці, виводячи тільки 10 користувачів на сторінці
  array.slice((page - 1) * 10, page * 10).forEach(addUserToTable);
}

//ПОШУК

//відображення результатів пошуку та оновлення фільтрів
function displaySearch() {
  const searchForm = document.querySelector(".search-form");
  const searchInput = searchForm.querySelector("input");
  const searchInfo = document.querySelector(".search-info");

  //додаємо слухача подій для форми пошуку
  searchForm.addEventListener("submit", (e) => {
    e.preventDefault();
    //перевіряємо, чи введено значення в поле пошуку
    if (searchInput.value) {
      //отримуємо результати пошуку для введеного значення
      let searchResults = searchObjectsByParameter(users, searchInput.value);
      console.log(searchResults);

      //виводимо інформацію про результати пошуку та оновлюємо фільтри
      searchInfo.hidden = false;
      resetFilters();
      handleFilters(searchResults);
      sortTable(searchResults);

      //виводимо інформацію про результати пошуку
      searchInfo.querySelector(".search-word").innerHTML = searchInput.value;
      searchInfo.querySelector(".number-of-matches").innerHTML =
        searchResults.length;
      searchInfo.querySelector(".matches-percentage").innerHTML =
        calculatePercentageMatching(users, searchInput.value);

      //додаємо слухача події для скидання результатів пошуку та відображення всіх вчителів
      searchInfo
        .querySelector("#remove-search")
        .addEventListener("click", () => {
          searchInfo.hidden = true;
          resetFilters();
          handleFilters(users);
          sortTable(users);
          console.log("remove search");
        });
      searchInput.value = "";
    }
  });
}

//ФОРМА ДОДАВАННЯ ВЧИТЕЛЯ

//обробка форми додавання нового вчителя
function handleForm() {
  //обчислення віку за датою народження
  function calculateAge(birthDate) {
    const today = new Date();
    const birth = new Date(birthDate);
    //розраховуємо вік
    let age = today.getFullYear() - birth.getFullYear();
    const monthDiff = today.getMonth() - birth.getMonth();

    //зменшуємо вік, якщо місяць народження ще не настав або якщо місяць народження вже настав, але дня народження ще не було
    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birth.getDate())
    ) {
      age--;
    }
    return age;
  }

  //отримання значень форми
  const fullName = document.getElementById("addName");
  const speciality = document.getElementById("addSpeciality");
  const country = document.getElementById("addCountry");
  const city = document.getElementById("addCity");
  const email = document.getElementById("addEmail");
  const phone = document.getElementById("addPhone");
  const birthDate = document.getElementById("addDate");
  const bgColor = document.getElementById("addBgColor");
  const notes = document.getElementById("addNotes");
  const form = document.querySelector(".dialog-main");

  //встановлює слухачі подій для полів форми (імені, міста, електронної пошти та телефону),
  //які викликають функції валідації при введенні даних
  fullName.addEventListener("input", () => {
    //перевірка, чи введено ім'я та чи починається воно з великої літери
    if (
      fullName.value &&
      fullName.value[0] === fullName.value[0].toUpperCase()
    ) {
      fullName.classList.remove("invalid");
    }
  });

  city.addEventListener("input", () => {
    //перевірка, чи введено місто та чи починається воно з великої літери
    if (city.value && city.value[0] === city.value[0].toUpperCase()) {
      city.classList.remove("invalid");
    }
  });

  email.addEventListener("input", () => {
    //перевірка, чи введена коректна електронна пошта
    if (email.value && validateEmail(email.value)) {
      email.classList.remove("invalid");
    }
  });

  phone.addEventListener("input", () => {
    console.log(phone.value);
    //перевірка, чи введений коректний номер телефону для вибраної країни
    if (phone.value && validatePhone(phone.value, country.value)) {
      phone.classList.remove("invalid");
    }
  });

  form.addEventListener("submit", (e) => {
    e.preventDefault();

    const sex = document.querySelector('input[name="sex"]:checked');

    //отримання значення note та форматуванням першої літери у велику
    let note = notes.value
      ? notes.value[0].toUpperCase() + notes.value.slice(1, notes.value.length)
      : "Note";

    //перевірка коректності введеної дати
    const inputDate = new Date(birthDate.value);
    const currentDate = new Date();

    if (inputDate > currentDate) {
      alert("Invalid birth date. Please enter a valid date.");
      return;
    }

    //обчислення віку
    const age = calculateAge(birthDate.value);
    //створення об'єкта з даними форми
    const teacher = {
      full_name: fullName.value,
      course: speciality.value,
      country: country.value,
      city: city.value,
      email: email.value,
      phone: phone.value,
      b_date: birthDate.value,
      gender: sex.value,
      bg_color: bgColor.value,
      note: note,
      age: age,
    };
    console.log(teacher);

    //якщо дані введено коректно, додаємо вчителя і відображаємо їх
    if (validateUser(teacher)) {
      console.log(teacher);
      fetch("http://localhost:3000/teachers", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        //дані для відправлення передаються у вигляді JSON-рядка
        body: JSON.stringify(teacher),
      })
        .then((response) => {
          if (response.ok) console.log("data posted successfully")
          return response.json();
        })
        .catch((error) => {
          console.error("Failed to add teacher:", error.message);
        });
        //додаємо нового вчителя до списку юзерів і на сторінку
        users.push(teacher);
        displayTeachers(users);
        handleFilters(users);
      form.reset();
      
    } else {
      //якщо дані введено некоректно, виводимо повідомлення про помилку
      let errorMessage = "Invalid data:\n";
      if (!(teacher.full_name[0] === teacher.full_name[0].toUpperCase())) {
        errorMessage += "Name must start with a capital letter\n";
        fullName.classList.add("invalid");
      }
      if (!(teacher.city[0] === teacher.city[0].toUpperCase())) {
        errorMessage += "City must start with a capital letter\n";
        city.classList.add("invalid");
      }
      if (!validateEmail(teacher.email)) {
        errorMessage += "Invalid email\n";
        email.classList.add("invalid");
      }
      if (!validatePhone(teacher.phone, teacher.country)) {
        errorMessage += "Invalid phone number\n";
        phone.classList.add("invalid");
      }
      alert(errorMessage);
    }
  }); 
}

//функція для виконання запиту до API та отримання списку користувачів
function fetchUsers(n) {
  //використовуємо Fetch API для виконання GET-запиту за вказаною URL-адресою
  return fetch(`https://randomuser.me/api/?results=${n}`)
    .then((response) => {
      //перевіряємо, чи відповідь сервера має статус 200 (OK)
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.json();
    })
    .then((data) => {
      //повертаємо масив результатів з отриманих даних
      return data.results;
    })
    .catch((error) => {
      console.error(error.message);
      throw error;
    });
}

async function init() {
  try {
    //отримуємо список користувачів
    users = await fetchUsers(50)
      .then((fetchedUsers) => {
        console.log("fetchedUsers:", fetchedUsers);
        return modifyUsersArray(fetchedUsers);
      })
      .catch((error) => {
        console.error("Failed to fetch users:", error.message);
      });

    console.log("users:", users);

    //додаємо слухач подій на кнопку moreButton для отримання додаткових юзерів
    document
      .getElementById("moreButton")
      .addEventListener("click", addTenMoreTeachers);

    //функції для відображення та обробки даних
    displayTeachers(users);
    displayFavoriteTeachers(users);
    handleFilters(users);
    sortTable(users);
    displaySearch();
    handleForm();
  } catch (error) {
    console.error(error.message);
  }
}

//додавання 10 додаткових юзерів
async function addTenMoreTeachers() {
  try {
    let newUsers = await fetchUsers(10);
    //розширюємо існуючий масив користувачів новими отриманими користувачами
    users = [...users, ...modifyUsersArray(newUsers)];

    //функції для відображення та обробки даних
    displayTeachers(users);
    displayFavoriteTeachers(users);
    handleFilters(users);
    sortTable(users);
    displaySearch();
    handleForm();
  } catch (error) {
    console.error(error.message);
  }
}

//викликаємо функцію init при завантаженні сторінки
window.onload = () => {
  init();
};
